"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSourceFile = exports.addModuleImportToModule = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const dependencies_1 = require("@schematics/angular/utility/dependencies");
const tasks_1 = require("@angular-devkit/schematics/tasks");
const change_1 = require("@schematics/angular/utility/change");
const typescript = require("typescript");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
function addPackageJsonDependencies() {
    return (host, context) => {
        const dependencies = [
            // { type: NodeDependencyType.Default, version: '~6.1.1', name: '@angular/elements' },
            // { type: NodeDependencyType.Default, version: '~1.1.0', name: '@webcomponents/custom-elements' },
            { type: dependencies_1.NodeDependencyType.Default, version: '0.0.5', name: '@sergey-nefedov/component-library' }
        ];
        dependencies.forEach(dependency => {
            (0, dependencies_1.addPackageJsonDependency)(host, dependency);
            context.logger.log('info', `✅️ Added "${dependency.name}" into ${dependency.type}`);
        });
        return host;
    };
}
function installPackageJsonDependencies() {
    return (host, context) => {
        context.addTask(new tasks_1.NodePackageInstallTask());
        context.logger.log('info', `🔍 Installing packages...`);
        return host;
    };
}
function addModuleToImports() {
    return (host, context) => {
        const moduleName = 'EditPanelModule';
        addModuleImportToModule(host, moduleName, '@sergey-nefedov/component-library');
        context.logger.log('info', `✅️ "${moduleName}" is imported`);
        return;
    };
}
function addModuleImportToModule(host, moduleName, src) {
    const modulePath = '/src/app/app.module.ts';
    const moduleSource = getSourceFile(host, modulePath);
    if (!moduleSource) {
        throw new schematics_1.SchematicsException(`Module not found: ${modulePath}`);
    }
    const changes = (0, ast_utils_1.addImportToModule)(moduleSource, modulePath, moduleName, src);
    const recorder = host.beginUpdate(modulePath);
    changes.forEach(change => {
        if (change instanceof change_1.InsertChange) {
            recorder.insertLeft(change.pos, change.toAdd);
        }
    });
    host.commitUpdate(recorder);
}
exports.addModuleImportToModule = addModuleImportToModule;
function getSourceFile(host, path) {
    const buffer = host.read(path);
    if (!buffer) {
        throw new schematics_1.SchematicsException(`Could not find file for path: ${path}`);
    }
    return typescript.createSourceFile(path, buffer.toString(), typescript.ScriptTarget.Latest, true);
}
exports.getSourceFile = getSourceFile;
// function addPolyfillToScripts(options: any) {
//   return (host: Tree, context: SchematicContext) => {
//     const polyfillName = 'custom-elements';
//     const polyfillPath = 'node_modules/@webcomponents/custom-elements/src/native-shim.js';
//
//     try {
//       const angularJsonFile = host.read('angular.json');
//
//       if (angularJsonFile) {
//         const angularJsonFileObject = JSON.parse(angularJsonFile.toString('utf-8'));
//         const project = options.project ? options.project : Object.keys(angularJsonFileObject['projects'])[0];
//         const projectObject = angularJsonFileObject.projects[project];
//         const scripts = projectObject.targets.build.options.scripts;
//
//         scripts.push({
//           input: polyfillPath
//         });
//         host.overwrite('angular.json', JSON.stringify(angularJsonFileObject, null, 2));
//       }
//     } catch (e) {
//       context.logger.log('error', `🚫 Failed to add the polyfill "${polyfillName}" to scripts`);
//     }
//
//     context.logger.log('info', `✅️ Added "${polyfillName}" polyfill to scripts`);
//
//     return host;
//   };
// }
function default_1(options) {
    return (0, schematics_1.chain)([
        options && options.skipPackageJson ? (0, schematics_1.noop)() : addPackageJsonDependencies(),
        options && options.skipPackageJson ? (0, schematics_1.noop)() : installPackageJsonDependencies(),
        options && options.skipModuleImport ? (0, schematics_1.noop)() : addModuleToImports(),
        // options && options.skipPolyfill ? noop() : addPolyfillToScripts(options)
    ]);
}
exports.default = default_1;
//# sourceMappingURL=index.js.map