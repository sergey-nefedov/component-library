import { chain, noop, Rule, SchematicContext, SchematicsException, Tree } from '@angular-devkit/schematics';
import {
  NodeDependency,
  NodeDependencyType,
  addPackageJsonDependency,
} from '@schematics/angular/utility/dependencies';
import { NodePackageInstallTask } from "@angular-devkit/schematics/tasks";
import { InsertChange } from '@schematics/angular/utility/change';
import typescript = require('typescript');
import { addImportToModule } from '@schematics/angular/utility/ast-utils';


function addPackageJsonDependencies(): Rule {
  return (host: Tree, context: SchematicContext) => {
    const dependencies: NodeDependency[] = [
      // { type: NodeDependencyType.Default, version: '~6.1.1', name: '@angular/elements' },
      // { type: NodeDependencyType.Default, version: '~1.1.0', name: '@webcomponents/custom-elements' },
      { type: NodeDependencyType.Default, version: '0.0.5', name: '@sergey-nefedov/component-library' }
    ];

    dependencies.forEach(dependency => {
      addPackageJsonDependency(host, dependency);
      context.logger.log('info', `✅️ Added "${dependency.name}" into ${dependency.type}`);
    });

    return host;
  };
}

function installPackageJsonDependencies(): Rule {
  return (host: Tree, context: SchematicContext) => {
    context.addTask(new NodePackageInstallTask());
    context.logger.log('info', `🔍 Installing packages...`);
    return host;
  };
}

function addModuleToImports(): Rule {
  return (host: Tree, context: SchematicContext) => {
    const moduleName = 'EditPanelModule';
    addModuleImportToModule(host, moduleName, '@sergey-nefedov/component-library');
    context.logger.log('info', `✅️ "${moduleName}" is imported`);
    return;
  };
}

export function addModuleImportToModule(host: Tree, moduleName: string, src: string) {
  const modulePath = '/src/app/app.module.ts';
  const moduleSource: any = getSourceFile(host, modulePath);

  if (!moduleSource) {
    throw new SchematicsException(`Module not found: ${modulePath}`);
  }

  const changes = addImportToModule(moduleSource, modulePath, moduleName, src);
  const recorder = host.beginUpdate(modulePath);

  changes.forEach(change => {
    if (change instanceof InsertChange) {
      recorder.insertLeft(change.pos, change.toAdd);
    }
  });

  host.commitUpdate(recorder);
}

export function getSourceFile(host: Tree, path: string): typescript.SourceFile {
  const buffer = host.read(path);
  if (!buffer) {
    throw new SchematicsException(`Could not find file for path: ${path}`);
  }
  return typescript.createSourceFile(path, buffer.toString(), typescript.ScriptTarget.Latest, true);
}

// function addPolyfillToScripts(options: any) {
//   return (host: Tree, context: SchematicContext) => {
//     const polyfillName = 'custom-elements';
//     const polyfillPath = 'node_modules/@webcomponents/custom-elements/src/native-shim.js';
//
//     try {
//       const angularJsonFile = host.read('angular.json');
//
//       if (angularJsonFile) {
//         const angularJsonFileObject = JSON.parse(angularJsonFile.toString('utf-8'));
//         const project = options.project ? options.project : Object.keys(angularJsonFileObject['projects'])[0];
//         const projectObject = angularJsonFileObject.projects[project];
//         const scripts = projectObject.targets.build.options.scripts;
//
//         scripts.push({
//           input: polyfillPath
//         });
//         host.overwrite('angular.json', JSON.stringify(angularJsonFileObject, null, 2));
//       }
//     } catch (e) {
//       context.logger.log('error', `🚫 Failed to add the polyfill "${polyfillName}" to scripts`);
//     }
//
//     context.logger.log('info', `✅️ Added "${polyfillName}" polyfill to scripts`);
//
//     return host;
//   };
// }

export default function(options: any): Rule {
  return chain([
    options && options.skipPackageJson ? noop() : addPackageJsonDependencies(),
    options && options.skipPackageJson ? noop() : installPackageJsonDependencies(),
    options && options.skipModuleImport ? noop() : addModuleToImports(),
    // options && options.skipPolyfill ? noop() : addPolyfillToScripts(options)
  ]);
}
