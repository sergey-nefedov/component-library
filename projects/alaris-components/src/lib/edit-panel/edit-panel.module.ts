import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditPanelComponent} from "./edit-panel.component";
import {BrowserModule} from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {EditPanelDirective} from "./edit-panel.directive";

@NgModule({
  declarations: [EditPanelComponent, EditPanelDirective],
  imports: [BrowserModule, CommonModule, BrowserAnimationsModule],
  exports: [EditPanelComponent, EditPanelDirective],
})
export class EditPanelModule {}
