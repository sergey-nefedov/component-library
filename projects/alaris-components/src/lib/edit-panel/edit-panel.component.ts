import { Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { EditPanelService } from './edit-panel.service';

type Size = 'sm' | 'md' | 'lg';

@Component({
  selector: 'alaris-edit-panel',
  templateUrl: './edit-panel.component.html',
  styleUrls: ['./edit-panel.component.scss'],
  animations: [
    trigger(
      'inAnimation',
      [
        transition(
          ':enter',
          [
            style({ transform: 'translateX(100%)', opacity: '0' }),
            animate('350ms ease-out',
              style({ transform: 'translateX(0%)', opacity: '1' }))
          ]
        )
      ]
    ),
    trigger(
      'outAnimation',
      [
        transition(
          ':leave',
          [
            style({ transform: 'translateX(0%)', opacity: '1' }),
            animate('100ms ease-in',
              style({ transform: 'translateX(100%)', opacity: '0' }))
          ]
        )
      ]
    )
  ]
})
export class EditPanelComponent implements OnInit, OnDestroy {
  @Input() opened: unknown | undefined = undefined;
  @Output() closed = new EventEmitter<void>();

  @HostBinding('attr.data-size')
  @Input()
  public size: Size = 'sm';

  constructor(
    private editPanel: EditPanelService
  ) {
  }

  ngOnInit(): void {
    this.editPanel.opened.next(true);
  }

  ngOnDestroy(): void {
    this.editPanel.opened.next(false);
    this.closed.emit();
  }

}
