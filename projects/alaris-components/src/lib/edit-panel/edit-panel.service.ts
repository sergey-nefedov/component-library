import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditPanelService {

  public opened = new Subject<boolean>();

  constructor() {
  }

}
