import { Directive, ElementRef, Input, OnInit } from '@angular/core';

enum ContentType {
  HEADER = 'header',
  CONTENT = 'content',
  FOOTER = 'footer'
}

@Directive({
  selector: '[alarisEditPanel]'
})
export class EditPanelDirective implements OnInit {
  @Input('alarisEditPanel') public contentType!: string;
  constructor(private el: ElementRef) { }

  public ngOnInit(): void {
    switch (this.contentType) {
      case ContentType.HEADER:
        this.el.nativeElement.classList.add('edit-panel-title', 'simple-title');
        break;
      case ContentType.CONTENT:
        this.el.nativeElement.classList.add('edit-panel-content', 'edit-panel-hr', 'simple-title');
        break;
      case ContentType.FOOTER:
        this.el.nativeElement.classList.add('edit-panel-footer', 'edit-panel-hr');
        break;
    }
  }
}
